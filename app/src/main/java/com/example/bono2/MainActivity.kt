package com.example.bono2

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.storage.FirebaseStorage
import java.io.ByteArrayOutputStream
import java.util.*


class MainActivity : AppCompatActivity() {
    val storage = FirebaseStorage.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    val REQUEST_IMAGE_CAPTURE = 1

    fun dispatchTakePictureIntent(view : View) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        // Create a storage reference from our app
        val storageRef = storage.getReference()
        val tiem = Date()
// Create a reference to "mountains.jpg"
        val mountainsRef = storageRef.child("student"+tiem+".jpg")

// Create a reference to 'images/mountains.jpg'
        val mountainImagesRef = storageRef.child("images/student.jpg")

// While the file names are the same, the references point to different files
        mountainsRef.getName().equals(mountainImagesRef.getName())    // true
        mountainsRef.getPath().equals(mountainImagesRef.getPath())    // false
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val extras = data!!.extras
            val bitmap = extras!!.get("data") as Bitmap

            // Get the data from an ImageView as bytes
            val baos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val data = baos.toByteArray()

            val uploadTask = mountainsRef.putBytes(data)
            uploadTask.addOnFailureListener(OnFailureListener {
                // Handle unsuccessful uploads
            }).addOnSuccessListener(OnSuccessListener<Any> {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                // ...
            })
        }
    }
}
