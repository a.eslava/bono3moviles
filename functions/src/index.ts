import * as functions from 'firebase-functions';

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.firestore();


exports.asd = functions.firestore
    .document('attendance/{dateid}/students/{codigo}')
    .onCreate((snap, context) => {
		const codigo= context.params.codigo
		return db.doc('studentsList/'.concat(codigo)).get()
		.then((data : any) => {
			const nuevaAtendance = data.data().cantidad+1
			const nombre = data.data().nombre
			const codigo1 = data.data().codigo
			const mac = data.data().mac
			return db.doc('studentsList/'.concat(codigo)).set({cantidad: nuevaAtendance, nombre: nombre, codigo : codigo1, mac : mac})
		});
    });


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });



// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
